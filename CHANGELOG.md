# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## UNRELEASED

---

## `v1.1.0` _15/12/2021_

#Changed
- **Se ha actualizado el estilo y la lógica del menú con indicador circular**
  - _Ahora cuando se hace **"hover"** sobre una opción el indicador no se mueve, pero si desaparece el icono y aparece el texto._
  - _El indicador sólo se mueve cuando se clica en una opción del menú._
  

---

## `v1.0.0` _15/12/2021_

### Changed
- **Se ha cambiado el nombre de la clase del indicador circular del menú.**
  - _Antes se llamaba `bubble-indicator` y ahora se llama `circular-indicator`._
  

- **Se ha actualizado el fichero _"nav.css"_ para que el movimiento del indicador circular del menú sea más suave.**

### Feature
- **Se ha creado el fichero _"nav.js"_ con la lógica que va a llevar el menú con indicador circular.**
- **Se ha creado el fichero _"nav.css"_ con el estilo que va a llevar el menú con indicador circular.**
- **Se ha creado el fichero _"main.css"_ con el estilo principal que va a llevar el sitio web.**
- **Se ha creado la estructura básica del menu con HTML.**
- **Se ha añadido la librearía CSS de [fontawesome](https://fontawesome.com)**

---

## `v0.0.0` _14/12/2021_

### Feature
- **Se ha creado la estructura del proyecto**
  - _Se ha creado la estructura de carpetas del proyecto creando ficheros **".gitkeep"**._
  - _Se ha creado el fichero HTML principal donde irá la estructura HTML del proyecto._  
  

- **Se ha inicializado el proyecto**
  - _Se ha creado el fichero **".gitignore"** para no subir al repositorio la carpeta del ide._
  - _Se ha creado el fichero **"CHANGELOG.md"** para registrar todos los cambios notables para cada versión del proyecto de maenra ordenada cronológicamente._
  - _Se ha creado el fichero **"README.md"** con alguna información relevante al proyecto._

---