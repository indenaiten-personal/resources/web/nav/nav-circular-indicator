// -------------------------------------------------- \\
// ---| NAV JS |--- \\
// -------------------------------------------------- \\

//Run when the window is loaded
window.addEventListener( "load", function(){

   //Constants
   const NAV = "#nav-circular-indicator";
   const OPTION = `${ NAV } .option`;
   const CIRCULAR_INDICATOR = `${ NAV } .circular-indicator`;
   const ACTIVE_CLASS = "active";

   //Get all options to add the logic
   document.querySelectorAll( OPTION ).forEach( function( target, index ){
      //Set "active" class when it's clicked on option element
      target.addEventListener( "click", function(){
         //Remove all "active" class from all options
         document.querySelectorAll( OPTION ).forEach( function( target ){
            if( target.classList.contains( ACTIVE_CLASS ) ) {
               target.classList.remove( ACTIVE_CLASS );
            }
         });

         //Set "active" class in target option and move the circular indicator to de target option position
         this.classList.add( ACTIVE_CLASS );
         document.querySelector( CIRCULAR_INDICATOR ).style.left = `${ index*100 }px`;
      });
   });

});

// -------------------------------------------------- \\
// ---| END NAV JS |--- \\
// -------------------------------------------------- \\